// CRUD Operations

// 1. Creating Documents (Creating a Table)
// [Insert One]
db.users.insert({
	firstName: "Ely",
	lastName: "Buendia",
	age: 50,
	contact: {
		phone: "5347786",
		email: "ely@eraserheads.com"
	},
	courses: ["CSS", "Javascript", "Python"],
	department: "none"
})
// [Insert One END]

// [Insert Many]
db.users.insertMany([
	{
		firstName: "Chito",
		lastName: "Miranda",
		age: 43,
		contact: {
			phone: "5347786",
			email: "chito@parokya.com"
		},
		courses: ["Python", "React", "PHP"],
		department: "none"
	},
	{
		firstName: "Francis",
		lastName: "Magalona",
		age: 61,
		contact: {
			phone: "5347786",
			email: "francisM@email.com"
		},
		courses: ["React", "Laravel", "SASS"],
		department: "none"
	}
])
// [Insert Many END]


// 2. Finding Documents (Finding a Document)
// [Find All]
db.users.find()
// [Find All END]

// [Find One]
db.users.find({ firstName: "Francis", age: 61 })
// [Find One END]


// 3. Deleting Documents (Deleting a Document)
// [Delete One]
db.users.deleteOne({
	firstName: "Ely"
})
// [Delete One END]

// [Delete Many]
db.users.deleteMany({
	department: "none"
})
// [Delete Many END]


// 4. Updating Documents (Updating a document)
db.users.updateOne(
	{
		firstName: "Chito"
	},
	{
		$set: {
			lastName: "Esguerra"
		}
	}
)

